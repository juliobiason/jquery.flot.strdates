About strDates
==============

strDates is a [flot](https://github.com/flot/flot) plugin to accept dates
in string format instead of miliseconds since epoch (Jan 1st, 1970).

Date format
-----------

Currently, strDates only accept dates in the format "YYYY-MM-DD HH:MM:SS".
Dates in the original format (miliseconds since epoch) still work and won't
be handled by the plugin.

Options
-------

This plugin have no options.