(function ($) {
	// convert a string to its epoch equivalent, if the format string is in
	// the YYYY-MM-DD HH:MM:SS format. returns the original value if it
	// doesn't match or produces an invalid date.
	function convert(str) {
		if(!isNaN(str))
			// already a number
			return str;

		var m = str.match(/(\d+)[-/\.](\d+)[-/\.](\d+) (\d+)[:\.](\d+)[:\.](\d+)/)
		if((m === null) || (m.length != 7))
			// the original string and the 6 matches
			return str;

		// return Date.UTC(m[1], m[2]-1, m[3], m[4], m[5], m[6]);
		var d = new Date(m[1], m[2]-1, m[3], m[4], m[5], m[6]);
		return d.getTime();
	}

	// ajust elements in the series
	function adjustSeries(plot, series, data, datapoints) {
		if(series.xaxis.options.mode != "time")
			return;

		var i, d;
		for (i = 0, d; i < data.length; ++i) {
			data[i][0] = convert(data[i][0]);
		}
	}

	// adjust the grid markings. this is kinda dangerous 'cause we may
	// change the markings even when none of the series use time
	function adjustGrid(plot, options) {
		if(!options.grid.markings)
			return;		// no markings

		var m = options.grid.markings,
			i, from, to, d
		for (i = 0; i < m.length; ++i) {
			if(!m[i].xaxis)
				continue;

			to = m[i].xaxis.to;
			if(m[i].xaxis.from) {
				options.grid.markings[i].xaxis.from = convert(m[i].xaxis.from);
			}

			if(m[i].xaxis.to) {
				options.grid.markings[i].xaxis.to = convert(m[i].xaxis.to);
			}
		}

		if(!options.xaxis)
			return;

		// textual axis info
		if(options.xaxis.min) {
			options.xaxis.min = convert(options.xaxis.min);
		}

		if(options.xaxis.max) {
			options.xaxis.max = convert(options.xaxis.max);
		}

		// calculated axis info
		for(i = 0; i < options.xaxes.length; ++i) {
			options.xaxes[i].max = convert(options.xaxes[i].max);
			options.xaxes[i].min = convert(options.xaxes[i].min);
		}
	}

	function init(plot) {
		plot.hooks.processOptions.push(adjustGrid);
		plot.hooks.processRawData.push(adjustSeries);
	}

	$.plot.plugins.push({
		init: init,
		options: {},
		name: 'strDates',
		version: '0.3'
	});
})(jQuery);
